//$PWD/node_modules/gulp/bin/gulp.js $@; exit $?
// ^ Works similar to a shebang line (#!node_modules/gulp/bin/gulp.js)
const gulp = require('gulp');
const posthtml = require('gulp-posthtml');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const merge = require('merge-stream');
const ts = require('gulp-typescript');

const postcssPlugins = [
    require('precss')({}),
    require('cssnano')({})
];
const postcssOptions = {};

const posthtmlPlugins = [
    require('posthtml-doctype')({ doctype: "HTML 5" }),
    require('posthtml-head-elements')({ headElements: 'src/html-head.json' }),
    require('posthtml-modules')({}),
    require('posthtml-favicons')({
        configuration: {
            appName: null,
            appDescription: null,

            developerName: 'Ma_124',
            developerURL: 'https://gitlab.com/Ma_124',

            background: '#fff',
            theme_color: '#fff',
        }
    }),
    require('posthtml-postcss')(postcssPlugins, postcssOptions),
    require('posthtml-minifier')({
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        html5: true,
        removeComments: true,
        removeEmptyAttributes: true,
        sortAttributes: true,
        sortClassName: true,
    })
];
const posthtmlOptions = {};

const tsOptions = {
    // TODO (+ tsconfig.json)
};

gulp.task('markup', function () {
    return gulp.src(['src/**/*.html'])
        .pipe(posthtml(posthtmlPlugins, posthtmlOptions))
        .pipe(gulp.dest('./dist'));
});

gulp.task('style', function () {
    return gulp.src(['src/**/*.css'])
        .pipe(sourcemaps.init())
        .pipe(postcss(postcssPlugins))
        .pipe(concat('app.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('script', function () {
    return merge(
        gulp.src(['src/**/*.ts', '!src/**/*.html.ts'])
            .pipe(sourcemaps.init())
            .pipe(ts(tsOptions))
            .pipe(babel())
            .pipe(concat('app.min.js'))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('./dist')),
        gulp.src(['src/**/*.html.ts'])
            .pipe(sourcemaps.init())
            .pipe(ts(tsOptions))
            .pipe(babel())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('./dist')),
    );
});

gulp.task('asset', function () {
   return gulp.src(['assets/**/*'])
       .pipe(gulp.dest('./dist'))
});

gulp.task('build', ['asset', 'style', 'script', 'markup']);

gulp.task('watch', ['asset', 'style', 'script', 'markup'], function () {
    gulp.watch('src/**/*.html', ['markup']);
    gulp.watch('src/**/*.css', ['style']);
    gulp.watch('src/**/*.ts', ['script']);
    gulp.watch(['src/**', '!src/**/*.html', '!src/**/*.css', '!src/**/*.ts'], ['build']);
    gulp.watch('assets', ['asset']);
});

gulp.task('default', ['build']);
