# Web Quick Start

## [`/assets/`](assets)
Place your static assets here. The `asset` task will copy them to `/dist`.

## `/dist/`
This is the output directory you can serve.

## [`/src/`](src)
Put your source files in here (`.ts`, `.html.ts`, `.css`, `.html`).

## [`/favicon.png`](favicon.png)
The [posthtml-favicons](https://github.com/mohsen1/posthtml-favicons) plugin will use this file to generate your favicons.

## [`/gulpfile.js`](gulpfile.js)
See [Gulp](https://gulpjs.com/)
### Tasks
#### Markup
This task will pipe all your `.html` files through [posthtml](#posthtml) and put the result into `/dist/`.

#### Style
This task will pipe all your `.css` files through [postcss](#postcss) and concatenates them to `/dist/app.min.css`.

#### Script
This task will pipe all your `.ts` files through [tsc](https://www.typescriptlang.org/) and [babel](https://babeljs.io/) and will concatenate every file not ending in `.html.ts` to `/dist/app.min.js`.

#### Asset
This task will copy all files in [`/assets`](assets) to `/dist/`
